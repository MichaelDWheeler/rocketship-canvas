﻿# boilerplate-gulp-sass
This is a boilerplate that has gulp and sass already configured. You should not make changes to this template unless you are trying to make changes for all future works from this template

## Cloning this template
Duplicating this template is a real pain in the butt. Visual Studio does not have an easy way to simply clone the folder and files. Names need to be changes in multiple places. For the correct way to do this watch the video at https://www.youtube.com/watch?v=uqXYK4q54XE

## Required installations
1. NodeJS - https://nodejs.org/en/
2. Ruby - http://www.ruby-lang.org/en/downloads/
3. Compass - http://compass-style.org/install/

## Getting compass started
Open the cmd window at the public directory. Type without quotes "compass watch". This will cause compass to create your css files from the scss file located in the sass directory. The command window needs to be at the public directory and not root because the sass folder is located in it.

## Getting gulp started
Open another cmd window at the root directory. Again without quotes you will type the following. If you are not building for production type "gulp", if you are building for production type "gulp prod". This will take the files from the public folder and move it to the dist folder. If you are building for production it will minify the javascript files, otherwise it will simply concat them to make it easier to debug.

## Modify the files in the public folder.
The folders in the public directory contain the files you can modify. It should be self explanatory. The sass folder contains the _custom.scss that needs to be modified for your css. If you started compass and gulp correctly, when you save it will move the file to the dist folder. 
