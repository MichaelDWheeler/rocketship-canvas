console.log(process.env.NODE_ENV);

var gulp = require('gulp');
var cleanCss = require('gulp-clean-css');
var concat = require('gulp-concat');
var minifyHtml = require('gulp-minify-html');
var path = require('./config');
var uglify = require('gulp-uglify');
var clean = require('del');
var debug = process.env.NODE_ENV != "production";


gulp.task('clean', function (done) {
    return clean([path.gulp.paths.dist], done);
});

gulp.task('scripts', function () {
    return gulp.src([
            path.gulp.paths.src + '/js/*.js',
        ])
        .pipe(concat('all.min.js'))
        .pipe(gulp.dest(path.gulp.paths.dist));
});

gulp.task('css', function () {
    return gulp.src(path.gulp.paths.src + '/stylesheets/*.css')
        .pipe(cleanCss())
        .pipe(gulp.dest(path.gulp.paths.dist));
});

gulp.task('html', function () {
    return gulp.src(path.gulp.paths.src + '/html/*.html')
        .pipe(minifyHtml())
        .pipe(gulp.dest(path.gulp.paths.dist));
});

gulp.task('audio', function () {
    return gulp.src(path.gulp.paths.src + '/audio/*.mp3')
        .pipe(gulp.dest(path.gulp.paths.dist));
});

gulp.task('images', function () {
    return gulp.src(path.gulp.paths.src + '/images/**')
        .pipe(gulp.dest(path.gulp.paths.dist));
});

gulp.task('buildApp', gulp.series('html', 'css', 'scripts', 'audio', 'images'));

gulp.task('build', function () {
    gulp.start('buildApp');
});



console.log('Finished gulp tasks');