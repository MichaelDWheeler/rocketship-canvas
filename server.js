var express = require('express');
var app = express();

app.use(express.static('dist'));

app.listen(process.env.port || 1337);
console.log('Port is listening');