//ctx.fillStyle = color || hex || rgba() || gradient || pattern

// *** CREATING LINEAR GRADIENT
//var g1 = ctx.createLinearGradient(0, 0, 200, 0);
//g1.addColorStop(0, "red");
//g1.addColorStop(0.5, "blue");
//g1.addColorStop(1, "black");
//ctx.fillStyle = g1;
//ctx.strokeStyle = "red";
//ctx.lineWidth = 5;
//ctx.fillRect(0, 0, 200, 200);
//ctx.strokeRect(0, 0, 200, 200);

// *** CREATING RADIAL GRADIENT
//var g2 = ctx.createRadialGradient(350, 100, 0, 350, 100, 200);
//g2.addColorStop(0, "red");
//g2.addColorStop(1, "blue");
//ctx.fillStyle = g2;
//ctx.fillRect(250, 0, 200, 200);
//ctx.strokeRect(250, 0, 200, 200);

// *** DIFFERENT METHODS FOR LINES THAT JOIN
//ctx.beginPath();
//ctx.moveTo(150, 150);
//ctx.lineTo(240, 240);
//ctx.lineTo(300, 40);
//ctx.lineWidth = 10;
//ctx.lineCap = "round"
//ctx.lineJoin = "round";
//ctx.miterLimit = 5;
//ctx.strokeStyle = "red";
//ctx.setLineDash([30]);
//ctx.lineDashOffset = 40;
//ctx.stroke();

// *** BUILDING AND DRAWING PATHS
//ctx.moveTo(50, 50);
//ctx.lineTo(250, 50);
//ctx.lineTo(250, 150);
//ctx.lineTo(50, 150);
//ctx.fillStyle = "red";
//ctx.lineWidth = "10";
//ctx.fill();
//ctx.closePath();
//ctx.stroke();

/// *** CLIPS ONE DRAWING INTO ANOTHER
//ctx.rect(50, 50, 100, 100);
//ctx.clip();
//ctx.rect(0, 0, 100, 100);
//ctx.stroke();

// *** CREATES A CIRCLE
//ctx.arc(150, 150, 100, 0, Math.PI * 2, false);
//ctx.fillStyle = "yellow";
//ctx.fill();
//ctx.stroke();

// *** MAKES A RECTANGLE WITH ROUNDED EDGES
//ctx.beginPath();
//ctx.moveTo(150, 50);
//ctx.lineTo(400, 50);
//ctx.arcTo(450, 50, 450, 100, 50);
//ctx.lineTo(450, 200);
//ctx.arcTo(450, 250, 350, 250, 50);
//ctx.lineTo(150, 250);
//ctx.arcTo(100, 250, 100, 200, 50);
//ctx.lineTo(100, 100);
//ctx.arcTo(100, 50, 150, 50, 50);
//ctx.fillStyle = "red";
//ctx.fill();
//ctx.lineWidth = "10";
//ctx.stroke();


// *** CREATE A QUADRATIC CURVE
//Quadratic curve is actually really simple. It takes the following arguments.
//controlPointX is the position where the outermost area of the curve will be on the x axis, the same goes for y. It will create the curve relative to your end of your current path. The x,y arguments will be where it ends.
//ctx.beginPath();
//ctx.moveTo(100, 100);
//ctx.lineTo(400, 100);

//ctx.quadraticCurveTo(controlPointX,controlPointY, x, y);
//ctx.quadraticCurveTo(0, 150, 100, 100);

//ctx.bezierCurveTo(cp1x, cp1y, cp2x, cp2y, x, y);
//ctx.bezierCurveTo(400, 250, 500, 200, 700, 200);

//ctx.drawImage(imageVariable, x, y);
//ctx.drawImage(myPic, 20, 20, 100,100);

// *** DRAWING TEXT
// fillText(text, x, y, maxWidth)
// strokeText(text, x, y, maxWidth)
//ctx.font = "400 56px Arial, sans-serif";
//ctx.textAlign = "start"; // start, end, left, right, center
//ctx.textBaseline = "top" // top, middle, bottom, hanging, alphabetic, ideographic
//ctx.strokeStyle = "red";
//ctx.fillStyle = "#FC0";
//ctx.fillText("Hello World!", 100, 100);
//ctx.strokeText("Hello World!", 100, 100);

// ***ADDING SHADOWS
//ctx.shadowColor = color || hex || rgba()
//ctx.shadowOffsetX = positive or negative number
//ctx.shadowOffsetY = positive or negative number
//ctx.shadowBlue = positive number
//ctx.fillStyle = "#FC0";
//ctx.shadowColor = "rgba(0, 0, 0, 0.5)";
//ctx.shadowBlur = "15";
//ctx.fillRect(300, 300, 200, 200);
//ctx.fillRect(100, 100, 200, 200);

// ***TRANSFORMATION EFFECTS
//ctx.fillStyle = "#FC0";
//ctx.fillRect(50, 200, 100, 100);
//ctx.scale(2, 2); Scales the entire canvas, so x and y positions will also be scaled
//ctx.rotate(.17); Rotates from the top left perspective of canvas, entire canvas rotates after this method
//ctx.translate(100, 50); moves drawing based on x y coordinates
//ctx.transform(xScale, ySkew, xSkew, yScale, xTrans, yTrans)
//xScale scales on x-axis, ySkew skews the image on the y-axis, xSkew skews it on x-axis, yScale scales the image on y-axis, xTrans moves the image over the number of pixels, and yTrans moves the image down the number of pixels. Use negative values to go the opposite way.
// ** default transform matrix is (1,0,0,1,0,0)
//ctx.setTransform - resets transform to whatever values you enter. If you were to use the ctx.transform it would apply the new values to the previously set values which would give you unpredictable results
//ctx.setTransform(1,0,0,1,30,0)
//ctx.fillRect(200, 200, 100, 100);

// ***PIXEL MANIPULATION
//var pic = new Image();
//pic.src = "pattern.png";
//ctx.drawImage(pic, 0, 0, 500, 500);
//ctx.fillStyle = "blue";
//ctx.fillRect(50, 50, 100, 100);
//ctx.fillStyle = "red";
//ctx.fillRect(100, 100, 100, 100);
//var src = ctx.getImageData(x, y, width, height) returns image object that coorisponds to the image contained within the arguments.
//var src = ctx.getImageData(50, 50, 200, 200);
//ctx.putImageData(src, 250, 20); puts the image that was returned from ctx.getImageData (src) and puts it on canvas as x and y coordinates
//var copy = ctx.createImageData(src.width, src.height); You can also only enter one argument such as "src" which will cause createImageData to get the width and height from the object.

//for (var i = 0; i < copy.data.length; i++) {
//    copy.data[i] = src.data[i];
//}

//ctx.putImageData(copy, 200, 100);

// *** Compositing 
//deals with setting global transparency, asset stacking controlling how your assets will interact with one another.
//Global alpha is 0.0 - 1.0
//ctx.globalAlpha = 0.5; - both squares will be set as it is read first
//ctx.globalCompositionOperation = source-atop || source-in || source-out || source-over (default) || destination-atop || destination-in || destination-out || destination-over || lighter || copy || xor 
//ctx.fillStyle = "red";
//ctx.fillRect(50, 50, 100, 100);
//ctx.globalCompositeOperation = "xor";
//ctx.fillStyle = "blue";
//ctx.fillRect(100, 100, 100, 100);

// *** DYNAMIC CENTERING AND ALIGNMENT
//ctx.fillStyle = "red";
//var rectW = 100;
//var rectH = 100;
//var rectX = (ctx.canvas.width / 2) - (rectW / 2);
//var rectY = (ctx.canvas.height / 2) - (rectH / 2);
//ctx.fillRect(rectX, rectY, rectW, rectH);

// ***ANIMATION AND APP INITILIZATION

//var ctx = document.getElementById('my-canvas').getContext('2d');
//var cW = ctx.canvas.width;
//var cH = ctx.canvas.height;
//var animateInterval = setInterval(animate, 30);
//var y = 0;
//var x = 0;

//var RectObj = function (x, y, width, height, color, speed) {
//    this.x = x;
//    this.y = y;
//    this.width = width;
//    this.height = height;
//    this.color = color;
//    this.speed = speed;

//    this.move = function () {
//        this.x += this.speed;
//        if (this.x >= cW - (this.width / 2)) {
//            this.speed = -this.speed;
//        } else if (this.x <= 0 - (this.width / 2)) {
//            this.speed = -this.speed;
//        }
//    }
//    this.render = function () {
//        ctx.fillStyle = this.color;
//        ctx.strokeStyle = "#000";
//        ctx.fillRect(this.x, this.y, this.width, this.height);
//        ctx.strokeRect(this.x, this.y, this.width, this.height);
//        this.move();
//    };

//};

//var rectArray = [];

//var renderRects = function () {
//    for (var i = 0, length = rectArray.length; i < length; i++) {
//        rectArray[i].render();
//    }
//};

//var animate = function () {
//    ctx.clearRect(0, 0, cW, cH);
//    // draw between save and restore
//    ctx.save();

//    renderRects()

//    ctx.restore();
//};

//var drawRect = function () {
//    for (var i = 0, length = rectArray.length; i < length; i++) {
//        rectArray[i].render();
//    }
//};

//var createRectangles = function () {

//    var dimension = function (attribute) {
//        var sum = Math.random() * attribute;
//        return sum;
//    };

//    var randomColor = function () {
//        var color = '#' + (function co(lor) {
//            return (lor +=
//                [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 'a', 'b', 'c', 'd', 'e', 'f'][Math.floor(Math.random() * 16)])
//                && (lor.length == 6) ? lor : co(lor);
//        })('');

//        return color;
//    };

//    var xPos = cW - 50;
//    var yPos = cH - 50;
//    var width = 100;
//    var height = 100;
//    var speed = 8;

//    for (var i = 0, amount = Math.random() * 100; i < amount; i++) {
//        rectArray.push(new RectObj(dimension(xPos), dimension(yPos), dimension(width), dimension(height), randomColor(), dimension(speed)));
//    }
//    drawRect();
//};

//var init = function () {
//    createRectangles();
//    setInterval(animate, 30);
//};

//window.onload = function () {
//    init();
//};

// ***INTERACTIVITY
//(function () {

//    var ctx = document.getElementById('my-canvas').getContext('2d');
//    var cW = ctx.canvas.width;
//    var cH = ctx.canvas.height;
//    var i = 0;

//    var addListeners = function () {
//        var status = document.getElementById('status');
//        ctx.canvas.addEventListener('mousemove', function (e) {
//            if (e.clientX >= 141 && e.clientX <= 440 && e.clientY >= 148 && e.clientY <= 190) {
//                ctx.canvas.style.cursor = "pointer";
//            } else {
//                ctx.canvas.style.cursor = "default";
//            }
//            var xpos = e.clientX - ctx.canvas.offsetLeft;
//            var ypos = e.clientY - ctx.canvas.offsetTop;
//            var newText = "x Position: " + xpos + " || Y Position " + ypos;
//            status.textContent = newText;

//        });
//        ctx.canvas.addEventListener('click', function (e) {

//            var clicked = document.getElementById('click-counter');
//            if (e.clientX >= 141 && e.clientX <= 440 && e.clientY >= 148 && e.clientY <= 190) {
//                i++;
//                clicked.textContent = "Clicked: " + i;

//            }
//            var xpos = e.clientX - ctx.canvas.offsetLeft;
//            var ypos = e.clientY - ctx.canvas.offsetTop;
//            ctx.fillStyle = "orange";
//            ctx.fillRect(xpos, ypos, 50, 50);
//        });
//    };

//    var addText = function () {
//        ctx.font = "400 56px Arial, sans-serif";
//        ctx.textAlign = "start"; // start, end, left, right, center
//        ctx.textBaseline = "top" // top, middle, bottom, hanging, alphabetic, ideographic
//        ctx.strokeStyle = "red";
//        ctx.fillStyle = "#FC0";
//        ctx.fillText("Hello World!", 100, 100);
//        ctx.strokeText("Hello World!", 100, 100);

//    };
//    var init = function () {
//        addListeners();
//        addText();
//    };

//    window.onload = function () {
//        init();
//    };


//})();
