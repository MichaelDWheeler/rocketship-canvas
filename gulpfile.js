'use strict'
var gulp = require('gulp');
require('require-dir')('./gulp');

gulp.task('default', gulp.series('clean', 'buildApp'), function () {
    process.env.NODE_ENV = null;
    gulp.start('build');
});

gulp.task('prod', gulp.series('clean'), function () {
    process.env.NODE_ENV = "production";
    gulp.start('build');
});